//Imports
const fs = require("fs");
const request = require("request");
const CONFIG = require("./config.json");

//Download data
const url = "https://a.mapillary.com/v3/sequences?per_page=1000&usernames="+CONFIG.user+"&client_id="+CONFIG.apikey;

console.log("[INFO]", "Start downloading data");
request(url, (err, res, body) => {
	if(err) {
		console.error(err);
	}
	
	if(body) {
		console.log("[INFO]", "Parsing data");
		try {
			const data = JSON.parse(body);
			let count = 0;
			
			if(data.features && data.features.length > 0) {
				console.log("[INFO]", "Reading "+data.features.length+" sequences");
				
				const stream = fs.createWriteStream("userdata.json");
				
				stream.once("open", fd => {
					stream.write('{ "data": [\n');
					
					const filtered = data.features.filter(f => f.geometry.coordinates.length >= 10);
					
					filtered.forEach((f,i) => {
						stream.write(JSON.stringify(f.geometry.coordinates[0].reverse())+',\n');
						stream.write(JSON.stringify(f.geometry.coordinates[f.geometry.coordinates.length-1].reverse()));
						stream.write(i === filtered.length -1 ? '\n' : ',\n');
					});
					
					stream.write('] }\n');
					stream.end();
					
					console.log("[INFO]", "Done, data is available in userdata.json");
					console.log("[INFO]", "To see results, open index.html in your web browser");
				});
			}
			else {
				console.log("[INFO]", "No data for this user, are you sure of the username ?");
			}
		}
		catch(e) {
			console.error(e);
		}
	}
});
