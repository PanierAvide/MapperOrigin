# MapperOrigin

This is a small analysis on places interesting you, as a Mapillary user.

## Method

The method for this analysis is pretty simple :
- Export latest sequences you took on Mapillary (limited to 1000)
- Filter sequences having at least 10 pictures in it
- For these sequences, keep only start and end point
- Export these points in a file
- Display this file as a heatmap


## Run it on your own

To run this locally, clone this repository. You have to copy `config.example.json` file and name it `config.json`. Edit this file to set your username, and a Mapillary API key (can be obtained on their website).

Then, in the repo folder, run these commands :

```sh
npm install
npm run start
```

Finally, you can see results by opening `index.html` page with your web browser.
